/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.astetis.serialization.stream;

import com.astetis.serialization.DataInputStream;

import java.io.IOException;

public class BinaryInputStream implements DataInputStream {

    private byte[] buffer;
    private int offset;
    private int length;

    public BinaryInputStream(byte[] buffer, int offset, int length) {
        if (buffer == null) {
            throw new IllegalArgumentException("'buffer' must not be null");
        }
        if (offset < 0) {
            throw new IllegalArgumentException("'offset' must not be negative.");
        }
        if (length < 0) {
            throw new IllegalArgumentException("'length' must not be negative.");
        }
        if (buffer.length < offset + length) {
            throw new IllegalArgumentException("Inconsistent lengths, total: " + buffer.length + ", offset: " + offset + ", len: " + length);
        }
        this.buffer = buffer;
        this.offset = offset;
        this.length = offset + length;
    }

    @Override
    public boolean isEOF() {
        return length <= offset;
    }

    @Override
    public long readInt64() throws IOException {
        if (offset + 8 > length) {
            throw new IOException();
        }

        long block1 = buffer[offset + 3] & 0xFF;
        long block2 = buffer[offset + 2] & 0xFF;
        long block3 = buffer[offset + 1] & 0xFF;
        long block4 = buffer[offset] & 0xFF;

        long res1 = (block1) + (block2 << 8) + (block3 << 16) + (block4 << 24);
        offset += 4;

        long block5 = buffer[offset + 3] & 0xFF;
        long block6 = buffer[offset + 2] & 0xFF;
        long block7 = buffer[offset + 1] & 0xFF;
        long block8 = buffer[offset] & 0xFF;

        long res2 = (block5) + (block6 << 8) + (block7 << 16) + (block8 << 24);
        offset += 4;

        return res2 + (res1 << 32);
    }

    @Override
    public int readInt() throws IOException {
        if (offset + 4 > length) {
            throw new IOException();
        }
        int block1 = buffer[offset + 3] & 0xFF;
        int block2 = buffer[offset + 2] & 0xFF;
        int block3 = buffer[offset + 1] & 0xFF;
        int block4 = buffer[offset] & 0xFF;
        offset += 4;
        return (block1) + (block2 << 8) + (block3 << 16) + (block4 << 24);
    }

    @Override
    public byte[] readBytes(int count) throws IOException {
        if (count < 0) {
            throw new IOException("Count must not be negative.");
        }
        if (offset + count > length) {
            throw new IOException("Buffer overhead, max len: " + length + ", required len: " + (offset + count));
        }
        byte[] res = new byte[count];
        for (int i = 0; i < count; i++) {
            res[i] = buffer[offset++];
        }
        return res;
    }

    @Override
    public long readHeader() throws IOException {
        long index = 0, bufferData, resultValue = 0;

        while (true) {
            if (offset == length) {
                throw new IOException();
            }

            bufferData = buffer[offset++] & 0xFF;

            if ((bufferData & 0x80) != 0) {
                resultValue |= (bufferData & 0x7F) << index;
                index += 7;
                if (index > 70) {
                    throw new IOException();
                }
            } else {
                break;
            }
        }

        return resultValue | (bufferData << index);
    }

}
