/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.astetis.serialization.io;

import com.astetis.serialization.DataInputStream;
import com.astetis.serialization.StreamReader;
import com.astetis.serialization.StreamSerializable;
import com.astetis.serialization.FieldNotFoundException;
import com.astetis.serialization.StreamUtils;
import com.astetis.serialization.ValTypes;
import com.astetis.serialization.stream.BinaryInputStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BinaryStreamReader implements StreamReader {

    private final Map<Integer, Object> fields = new HashMap<>();

    public BinaryStreamReader(DataInputStream stream) throws IOException {
        Map<Integer, Object> fields = new HashMap<>();
        while (!stream.isEOF()) {
            long currentFlag = stream.readHeader();

            int index = (int) (currentFlag >> 3);
            int type = (int) (currentFlag & 0x7);

            ValTypes wireType = ValTypes.of(type).orElseThrow(() -> new IOException("Unknown field type #" + type));
            if (wireType == ValTypes.HEADER) {
                store(index, stream.readHeader(), fields);
            } else if (wireType == ValTypes.DELIMITED) {
                int size = (int) stream.readHeader();
                store(index, stream.readBytes(size), fields);
            } else if (wireType == ValTypes.INT64) {
                store(index, stream.readInt64(), fields);
            } else if (wireType == ValTypes.INT32) {
                store(index, stream.readInt(), fields);
            }
        }
        this.fields.putAll(fields);
    }

    private void store(int index, Object res, Map<Integer, Object> map) {
        if (map.get(index) != null) {
            if (map.get(index) instanceof List) {
                ((List) map.get(index)).add(res);
            } else {
                ArrayList<Object> list = new ArrayList<Object>();
                list.add(map.get(index));
                list.add(res);
                map.put(index, list);
            }
        } else {
            map.put(index, res);
        }
    }

    @Override
    public long readOptionalLong(int index) throws IOException {
        return readLong(index, 0);
    }

    @Override
    public int readOptionalInt(int index) throws IOException {
        return StreamUtils.convertToInt(readLong(index));
    }

    @Override
    public double readOptionalDouble(int index) throws IOException {
        return Double.longBitsToDouble(readOptionalLong(index));
    }

    @Override
    public boolean readOptionalBoolean(int index) throws IOException {
        return readOptionalLong(index) != 0;
    }

    @Override
    public byte[] readOptionalBytes(int index) throws IOException {
        return readByteArray(index, null);
    }

    @Override
    public String readOptionalString(int index) throws IOException {
        return StreamUtils.convertByteToString(readOptionalBytes(index));
    }

    @Override
    public long readLong(int index, long defValue) throws IOException {
        if (fields.containsKey(index)) {
            Object object = fields.get(index);
            if (object instanceof Long) {
                return (Long) object;
            }
        }
        return defValue;
    }

    @Override
    public double readDouble(int index) throws IOException {
        return Double.longBitsToDouble(readLong(index));
    }

    @Override
    public int readInt(int index, int defValue) throws IOException {
        return StreamUtils.convertToInt(readLong(index, defValue));
    }

    @Override
    public String readString(int index) throws IOException {
        return StreamUtils.convertByteToString(readByteArray(index));
    }

    @Override
    public String readString(int index, String defValue) throws IOException {
        return StreamUtils.convertByteToString(readByteArray(index));
    }

    @Override
    public byte[] readByteArray(int index) throws IOException {
        if (!fields.containsKey(index)) {
            throw new FieldNotFoundException("Unable to find field #" + index);
        }
        return readByteArray(index, null);
    }

    @Override
    public byte[] readByteArray(int index, byte[] defValue) throws IOException {
        if (fields.containsKey(index)) {
            Object val = fields.get(index);
            if (val instanceof byte[]) {
                return (byte[]) val;
            }
        }
        return defValue;
    }

    @Override
    public long readLong(int index) throws IOException {
        return readLong(index, 0);
    }

    @Override
    public int readInt(int index) throws IOException {
        return readInt(index, 0);
    }

    @Override
    public <T extends StreamSerializable> T readOptionalSerializable(int index, T defaultValue) throws IOException {
        byte[] data = readOptionalBytes(index);
        if (data == null) {
            return null;
        }
        return IOContext.fromByteArray(defaultValue, new BinaryInputStream(data, 0, data.length));
    }

    @Override
    public <T extends StreamSerializable> T readSerializable(int index, T defaultValue) throws IOException {
        byte[] data = readOptionalBytes(index);
        if (data == null) {
            throw new FieldNotFoundException();
        }
        return IOContext.fromByteArray(defaultValue, new BinaryInputStream(data, 0, data.length));
    }

    @Override
    public List<Integer> readIntegerList(int index) throws IOException {
        List<Long> src = readLongList(index);
        List<Integer> res = new ArrayList<>();
        for (Long val : src) {
            res.add(StreamUtils.convertToInt(val));
        }
        return res;
    }

    @Override
    public List<byte[]> readByteArrayList(int index) throws IOException {
        List<byte[]> res = new ArrayList<>();
        if (fields.containsKey(index)) {
            Object val = fields.get(index);
            if (val instanceof byte[]) {
                res.add((byte[]) val);
            } else if (val instanceof List) {
                List<Object> rep = (List) val;
                for (Object val2 : rep) {
                    if (val2 instanceof byte[]) {
                        res.add((byte[]) val2);
                    } else {
                        throw new IOException("Expected type: byte[], got " + val2.getClass().getSimpleName());
                    }
                }
            } else {
                throw new IOException("Expected type: byte[], got " + val.getClass().getSimpleName());
            }
        }
        return res;
    }

    @Override
    public List<Long> readLongList(int index) throws IOException {
        List<Long> res = new ArrayList<>();
        if (fields.containsKey(index)) {
            Object val = fields.get(index);
            if (val instanceof Long) {
                res.add((Long) val);
            } else if (val instanceof List) {
                List<Object> rep = (List) val;
                for (Object val2 : rep) {
                    if (val2 instanceof Long) {
                        res.add((Long) val2);
                    } else {
                        throw new IOException("Expected type: long, got " + val2.getClass().getSimpleName());
                    }
                }
            } else {
                throw new IOException("Expected type: long, got " + val.getClass().getSimpleName());
            }
        }
        return res;
    }

    @Override
    public List<String> readStringList(int index) throws IOException {
        List<byte[]> src = readByteArrayList(index);
        List<String> res = new ArrayList<>();
        for (byte[] val : src) {
            res.add(StreamUtils.convertByteToString(val));
        }
        return res;
    }

    @Override
    public <T extends StreamSerializable> List<T> readSerializableList(int index, List<T> defValue) throws IOException {
        List<T> res = new ArrayList<>();
        for (byte[] val : readByteArrayList(index)) {
            res.add(IOContext.fromByteArray(defValue.remove(0), new BinaryInputStream(val, 0, val.length)));
        }
        return res;
    }
}

