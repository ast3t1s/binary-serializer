/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.astetis.serialization.io;

import com.astetis.serialization.DataOutputStream;
import com.astetis.serialization.StreamSerializable;
import com.astetis.serialization.StreamWriter;
import com.astetis.serialization.ValTypes;
import com.astetis.serialization.stream.BinaryOutputStream;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BinaryStreamWriter implements StreamWriter {

    private static final int MAX_DATA_BLOCK_SIZE = 1024 * 1024;
    private static final int MAX_VECTOR_SIZE = 1024 * 1024;

    private final DataOutputStream outputStream;

    private final Map<Integer, Boolean> writtenFields = new HashMap<>();

    public BinaryStreamWriter(DataOutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Stream can not be null");
        }
        this.outputStream = outputStream;
    }

    @Override
    public void writeBytes(int index, byte[] value) throws IOException {
        if (value == null) {
            throw new IllegalArgumentException("Value must not be null");
        }
        if (value.length > MAX_DATA_BLOCK_SIZE) {
            throw new IllegalArgumentException("Unable to write mote than 1 Mb");
        }
        writeTag(index, ValTypes.DELIMITED);
        outputStream.writeBytes(value, 0, value.length);
    }

    @Override
    public void writeString(int index, String value) throws IOException {
        if (value == null) {
            throw new IllegalArgumentException("Value must not be null");
        }
        writtenFields.put(index, true);
        writeTag(index, ValTypes.DELIMITED);
        byte[] data = value.getBytes();
        outputStream.writeHeader(data.length);
        outputStream.writeBytes(data, 0, data.length);
    }

    @Override
    public void writeBool(int index, boolean value) throws IOException {
        writeTag(index, ValTypes.HEADER);
        outputStream.writeHeader((value ? 1 : 0) & 0xFFFFFFFF);
    }

    @Override
    public void writeInt(int index, int value) throws IOException {
        writeTag(index, ValTypes.HEADER);
        outputStream.writeHeader(value & 0xFFFFFFFF);
    }

    @Override
    public void writeDouble(int index, double value) throws IOException {
        writeTag(index, ValTypes.INT64);
        outputStream.writeInt64(Double.doubleToLongBits(value) & 0xFFFFFFFF);
    }

    @Override
    public void writeLong(int index, long value) throws IOException {
        writeTag(index, ValTypes.HEADER);
        outputStream.writeHeader(value & 0xFFFFFFFF);
    }

    @Override
    public void writeLongs(int index, Collection<Long> values) throws IOException {
        if (values == null) {
            throw new IllegalArgumentException("Values must not be null");
        }
        if (values.size() > MAX_VECTOR_SIZE) {
            throw new IllegalArgumentException("Too many values");
        }
        writtenFields.put(index, true);
        for (Long value : values) {
            if (value == null) {
                throw new IllegalArgumentException("Value must not be null");
            }
            writeTag(index, ValTypes.HEADER);
            outputStream.writeHeader(value & 0xFFFFFFFF);
        }
    }

    @Override
    public void writeIntegers(int index, Collection<Integer> values) throws IOException {
        if (values == null) {
            throw new IllegalArgumentException("Values must not be null");
        }
        if (values.size() > MAX_VECTOR_SIZE) {
            throw new IllegalArgumentException("Too many values");
        }
        writtenFields.put(index, true);
        for (Integer value : values) {
            if (value == null) {
                throw new IllegalArgumentException("Value must not be null");
            }
            writeTag(index, ValTypes.HEADER);
            outputStream.writeHeader(value & 0xFFFFFFFF);
        }
    }

    @Override
    public void writeBooleans(int index, Collection<Boolean> values) throws IOException {
        if (values == null) {
            throw new IllegalArgumentException("Values must not be null");
        }
        if (values.size() > MAX_VECTOR_SIZE) {
            throw new IllegalArgumentException("Too many values");
        }
        writtenFields.put(index, true);
        for (Boolean value : values) {
            if (value == null) {
                throw new IllegalArgumentException("Value must not be null");
            }
            writeBool(index, value);
        }
    }

    @Override
    public void writeStrings(int index, Collection<String> values) throws IOException {
        if (values == null) {
            throw new IllegalArgumentException("Values must not be null");
        }
        if (values.size() > MAX_VECTOR_SIZE) {
            throw new IllegalArgumentException("Too many values");
        }
        writtenFields.put(index, true);
        for (String value : values) {
            if (value == null) {
                throw new IllegalArgumentException("Value must not be null");
            }
            writeString(index, value);
        }
    }

    @Override
    public void writeByteArrays(int index, Collection<byte[]> values) throws IOException {
        if (values == null) {
            throw new IllegalArgumentException("Values can not be null");
        }
        if (values.size() > MAX_VECTOR_SIZE) {
            throw new IllegalArgumentException("Too many values");
        }
        for (byte[] data : values) {
            if (data == null) {
                throw new IllegalArgumentException("Too many values");
            }
            writeBytes(index, data);
        }
    }

    @Override
    public <T extends StreamSerializable> void writeSerializableCollection(int index, Collection<T> values) throws IOException {
        if (values == null) {
            throw new IllegalArgumentException("Values must not be null");
        }
        if (values.size() > MAX_VECTOR_SIZE) {
            throw new IllegalArgumentException("To many values");
        }
        writtenFields.put(index, true);
        for (T value : values) {
            if (value == null) {
                throw new IllegalArgumentException("Value must not be null");
            }
            writeSerializable(index, value);
        }
    }

    @Override
    public void writeUnmapped(int index, Object value) throws IOException {
        if (writtenFields.getOrDefault(index, false)) {
            return;
        }
        if (value instanceof Long) {
            writeLong(index, (Long) value);
        } else if (value instanceof byte[]) {
            writeBytes(index, (byte[]) value);
        } else if (value instanceof List) {
            for (Object o : (List) value) {
                if (o instanceof Long) {
                    writeLong(index, (Long) o);
                } else if (o instanceof byte[]) {
                    writeBytes(index, (byte[]) o);
                } else {
                    throw new IOException("Incorrect unmapped value in List");
                }
            }
        } else {
            throw new IOException("Incorrect unmapped value");
        }
    }

    @Override
    public <T extends StreamSerializable> void writeSerializable(int index, T value) throws IOException {
        if (value == null) {
            throw new IllegalArgumentException("Object must not be null");
        }
        writtenFields.put(index, true);
        writeTag(index, ValTypes.DELIMITED);
        DataOutputStream dataOutput = new BinaryOutputStream();
        BinaryStreamWriter streamWriter = new BinaryStreamWriter(dataOutput);
        value.serialize(streamWriter);
        byte[] data = dataOutput.toByteArray();
        outputStream.writeBytes(data, 0, data.length);
    }

    @Override
    public void writeRaw(byte[] raw) throws IOException {
        if (raw == null) {
            throw new IllegalArgumentException("Raw can not be null");
        }
        outputStream.writeBytes(raw, 0, raw.length);
    }

    private void writeTag(int index, ValTypes type) throws IOException {
        index = (index & 0xFFFF);
        if (index <= 0) {
            throw new IllegalArgumentException("Field Number must greater than zero");
        }
        long tag = ((long) (index << 3) | type.getTypeValue());
        outputStream.writeHeader(tag);
    }
}
