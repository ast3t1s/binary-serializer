/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import com.astetis.serialization.BinaryStreamObject;
import com.astetis.serialization.StreamReader;
import com.astetis.serialization.StreamWriter;

import java.io.IOException;

public class TestPojo extends BinaryStreamObject {

    private int intField;
    private long longField;
    private String stringField;
    private double doubleField;
    private byte byteField;

    public TestPojo() {

    }

    public TestPojo(int intField, long longField, String stringField, double doubleField, byte byteField) {
        this.intField = intField;
        this.longField = longField;
        this.stringField = stringField;
        this.doubleField = doubleField;
        this.byteField = byteField;
    }

    public int getIntField() {
        return intField;
    }

    public void setIntField(int intField) {
        this.intField = intField;
    }

    public long getLongField() {
        return longField;
    }

    public void setLongField(long longField) {
        this.longField = longField;
    }

    public String getStringField() {
        return stringField;
    }

    public void setStringField(String stringField) {
        this.stringField = stringField;
    }

    public double getDoubleField() {
        return doubleField;
    }

    public void setDoubleField(double doubleField) {
        this.doubleField = doubleField;
    }

    public byte getByteField() {
        return byteField;
    }

    public void setByteField(byte byteField) {
        this.byteField = byteField;
    }

    @Override
    public void parse(StreamReader reader) throws IOException {
        intField = reader.readInt(1);
        longField = reader.readLong(2);
        doubleField = reader.readDouble(3);
        stringField = reader.readString(4);

        //byteField = (byte) reader.readInt(5);
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        writer.writeInt(1, intField);
        writer.writeLong(2, longField);
        writer.writeDouble(3, doubleField);
        writer.writeString(4, stringField);
        //writer.writeInt(5, byteField);
    }
}
