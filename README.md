# Binary Serializer
[![MIT License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://opensource.org/licenses/MIT)
[![pipeline status](https://gitlab.com/ast3t1s/spring-session-r2dbc/badges/master/pipeline.svg)](https://gitlab.com/ast3t1s/binary-serializer/-/commits/master)
[![version](https://img.shields.io/badge/version-0.1.0-orange.svg?style=flat-square&logo=appveyor)](https://gitlab.com/ast3t1s/binary-serializer/-/commits/release)

Binary serializer is the lightweight, simple to use binary serialization.

### Maven configuration
```xml
<dependency>
  <groupId>com.astetis</groupId>
  <artifactId>binary-serializer</artifactId>
  <version>${version}</version>
</dependency>
```

Here is a quick teaser of an application using Spring Session R2dbc for more detail see docs:
```java
public class Application {
    
    public static void main(String[] args) {
		Person person = new Person();
        person.setId(12L);
        person.setAge(25);        
        person.setName("Name");
        person.setPhones(Collection.emptyList());

        byte[] data = IOContext.toByteArray(person);
        Person fromBytes = IOContext.fromByteArray(new Person(), data);
    }
}

public class Person implements StreamSerializable {
    
    private Long id;
    private String name;
    private int age;
    private List<String> phones;
    
    public Person() {}
    
    public void setId(Long id) {
        this.id = id;
    }   
    
    public void setName(String name) {
        this.name = name;
    }   
    
    public void setAge(int age) {
        this.age = age;
    }   

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }   

    public void parse(StreamReader reader) throws IOException {
        this.id = reader.readLong(1);
        this.name = reader.readString(2);
        this.age = reader.readInt(3);
        this.phones = reader.readStringList(4);
    }
    
    public void serialize(StreamWriter writer) throws IOException {
        writer.writeLong(1, id);
        writer.writeString(2, name);
        writer.writeInt(3, age);
        writer.writeStrings(4, phones);
    }   

}
```
## Building from Source
If you want to try out, Binary Serializer can be
easily build with the maven wrapper. You also need JDK >= 1.8.
```shell script
    $ ./mvnw clean install
```
If you want to build with the regular mvn you will need Maven

## License

Copyright 2020.

Licensed under the MIT License.


